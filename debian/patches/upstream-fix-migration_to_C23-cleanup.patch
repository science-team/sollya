Description: upstream - fix migration to C23 issues
 Clean up now obsoleted declarations/definitions, and
 rename true (resp., false) ttrue (resp., tfalse)
 in some tests to avoid collisions with the now
 predefined maro true (reps., false)
 (see <https://en.cppreference.com/w/c/language/bool_constant>).
 This patch closes Debian bug #1092402.
Origin: vendor, Debian
Forwarded: by-email
Author: Jerome Benoit <calculus@rezozer.net>
Last-Update: 2025-01-25

--- a/execute.h
+++ b/execute.h
@@ -80,7 +80,7 @@
 #include "general.h"
 #include "expression.h"
 
-extern int miniyyparse();
+extern int miniyyparse(void *);
 extern void startBuffer(char *str);
 extern void endBuffer(void);
 
--- a/signalhandling.h
+++ b/signalhandling.h
@@ -61,7 +61,7 @@
 void initSignalHandler(int nointeract);
 void blockSignals(int nointeract);
 void initSignalHandlerCounted(int nointeract);
-void blockSignalsCounted();
+void blockSignalsCounted(int nointeract);
 void deferSignalHandling();
 void resumeSignalHandling();
 
--- a/tests-lib/tlogical_and.c
+++ b/tests-lib/tlogical_and.c
@@ -13,40 +13,40 @@
 }
 
 int main(void) {
-  sollya_obj_t true, false, a, b, res;
+  sollya_obj_t ttrue, tfalse, a, b, res;
 
   sollya_lib_init();
   sollya_lib_install_msg_callback(callback, NULL);
 
-  true = sollya_lib_true();
-  false = sollya_lib_false();
+  ttrue = sollya_lib_true();
+  tfalse = sollya_lib_false();
 
-  a = sollya_lib_copy_obj(true);
-  b = sollya_lib_copy_obj(true);
+  a = sollya_lib_copy_obj(ttrue);
+  b = sollya_lib_copy_obj(ttrue);
   res = sollya_lib_and(a, b);
   sollya_lib_printf("%b and %b gives %b\n", a, b, res);
   sollya_lib_clear_obj(a);
   sollya_lib_clear_obj(b);
   sollya_lib_clear_obj(res);
 
-  a = sollya_lib_copy_obj(true);
-  b = sollya_lib_copy_obj(false);
+  a = sollya_lib_copy_obj(ttrue);
+  b = sollya_lib_copy_obj(tfalse);
   res = sollya_lib_and(a, b);
   sollya_lib_printf("%b and %b gives %b\n", a, b, res);
   sollya_lib_clear_obj(a);
   sollya_lib_clear_obj(b);
   sollya_lib_clear_obj(res);
 
-  a = sollya_lib_copy_obj(false);
-  b = sollya_lib_copy_obj(true);
+  a = sollya_lib_copy_obj(tfalse);
+  b = sollya_lib_copy_obj(ttrue);
   res = sollya_lib_and(a, b);
   sollya_lib_printf("%b and %b gives %b\n", a, b, res);
   sollya_lib_clear_obj(a);
   sollya_lib_clear_obj(b);
   sollya_lib_clear_obj(res);
 
-  a = sollya_lib_copy_obj(false);
-  b = sollya_lib_copy_obj(false);
+  a = sollya_lib_copy_obj(tfalse);
+  b = sollya_lib_copy_obj(tfalse);
   res = sollya_lib_and(a, b);
   sollya_lib_printf("%b and %b gives %b\n", a, b, res);
   sollya_lib_clear_obj(a);
@@ -62,8 +62,8 @@
   sollya_lib_clear_obj(res);
 
 
-  sollya_lib_clear_obj(true);
-  sollya_lib_clear_obj(false);
+  sollya_lib_clear_obj(ttrue);
+  sollya_lib_clear_obj(tfalse);
   sollya_lib_close();
   return 0;
 }
--- a/tests-lib/tlogical_negate.c
+++ b/tests-lib/tlogical_negate.c
@@ -12,21 +12,21 @@
 
 }
 int main(void) {
-  sollya_obj_t true, false, a, res;
+  sollya_obj_t ttrue, tfalse, a, res;
 
   sollya_lib_init();
   sollya_lib_install_msg_callback(callback, NULL);
 
-  true = sollya_lib_true();
-  false = sollya_lib_false();
+  ttrue = sollya_lib_true();
+  tfalse = sollya_lib_false();
 
-  a = sollya_lib_copy_obj(true);
+  a = sollya_lib_copy_obj(ttrue);
   res = sollya_lib_negate(a);
   sollya_lib_printf("the negation of %b gives %b\n", a, res);
   sollya_lib_clear_obj(a);
   sollya_lib_clear_obj(res);
 
-  a = sollya_lib_copy_obj(false);
+  a = sollya_lib_copy_obj(tfalse);
   res = sollya_lib_negate(a);
   sollya_lib_printf("the negation of %b gives %b\n", a, res);
   sollya_lib_clear_obj(a);
@@ -39,8 +39,8 @@
   sollya_lib_clear_obj(res);
 
 
-  sollya_lib_clear_obj(true);
-  sollya_lib_clear_obj(false);
+  sollya_lib_clear_obj(ttrue);
+  sollya_lib_clear_obj(tfalse);
   sollya_lib_close();
   return 0;
 }
--- a/tests-lib/tlogical_or.c
+++ b/tests-lib/tlogical_or.c
@@ -12,40 +12,40 @@
 
 }
 int main(void) {
-  sollya_obj_t true, false, a, b, res;
+  sollya_obj_t ttrue, tfalse, a, b, res;
 
   sollya_lib_init();
   sollya_lib_install_msg_callback(callback, NULL);
 
-  true = sollya_lib_true();
-  false = sollya_lib_false();
+  ttrue = sollya_lib_true();
+  tfalse = sollya_lib_false();
 
-  a = sollya_lib_copy_obj(true);
-  b = sollya_lib_copy_obj(true);
+  a = sollya_lib_copy_obj(ttrue);
+  b = sollya_lib_copy_obj(ttrue);
   res = sollya_lib_or(a, b);
   sollya_lib_printf("%b or %b gives %b\n", a, b, res);
   sollya_lib_clear_obj(a);
   sollya_lib_clear_obj(b);
   sollya_lib_clear_obj(res);
 
-  a = sollya_lib_copy_obj(true);
-  b = sollya_lib_copy_obj(false);
+  a = sollya_lib_copy_obj(ttrue);
+  b = sollya_lib_copy_obj(tfalse);
   res = sollya_lib_or(a, b);
   sollya_lib_printf("%b or %b gives %b\n", a, b, res);
   sollya_lib_clear_obj(a);
   sollya_lib_clear_obj(b);
   sollya_lib_clear_obj(res);
 
-  a = sollya_lib_copy_obj(false);
-  b = sollya_lib_copy_obj(true);
+  a = sollya_lib_copy_obj(tfalse);
+  b = sollya_lib_copy_obj(ttrue);
   res = sollya_lib_or(a, b);
   sollya_lib_printf("%b or %b gives %b\n", a, b, res);
   sollya_lib_clear_obj(a);
   sollya_lib_clear_obj(b);
   sollya_lib_clear_obj(res);
 
-  a = sollya_lib_copy_obj(false);
-  b = sollya_lib_copy_obj(false);
+  a = sollya_lib_copy_obj(tfalse);
+  b = sollya_lib_copy_obj(tfalse);
   res = sollya_lib_or(a, b);
   sollya_lib_printf("%b or %b gives %b\n", a, b, res);
   sollya_lib_clear_obj(a);
@@ -61,8 +61,8 @@
   sollya_lib_clear_obj(res);
 
 
-  sollya_lib_clear_obj(true);
-  sollya_lib_clear_obj(false);
+  sollya_lib_clear_obj(ttrue);
+  sollya_lib_clear_obj(tfalse);
   sollya_lib_close();
   return 0;
 }
