#include <sollya.h>
#include <string.h>

int main(void) {
  const char *s;
  char *t;
  sollya_lib_init();

  /* No need to free the returned char * */
  s = sollya_lib_get_help_text("foo");
  if (s) {
    sollya_lib_printf("Help text for 'foo':\n%s", s);
  }
  else {
    sollya_lib_printf("\"foo\" is an identifier.\n");
  }

  s = sollya_lib_get_help_text("remez");
  if (!s) {
      sollya_lib_printf("\"remez\" is an identifier.\n");
  }
  else {
    t = strstr(s, "remez");
    if (t && (t-s <= 10)) {
      sollya_lib_printf("'remez' help text really is about remez.\n");
    }
    else {
      sollya_lib_printf("'remez' help text is the following:\n%s", s);
    }
  }

  s = sollya_lib_get_help_text("for");
  if (!s) {
      sollya_lib_printf("\"for\" is an identifier.\n");
  }
  else {
    sollya_lib_printf("'for' help text is the following:\n%s", s);
  }

  sollya_lib_close();
  return 0;
}
