\subsection{rename}
\label{labrename}
\noindent Name: \textbf{rename}\\
\phantom{aaa}rename the free variable.\\[0.2cm]
\noindent Library name:\\
\verb|   void sollya_lib_name_free_variable(const char *)|\\[0.2cm]
\noindent Usage: 
\begin{center}
\textbf{rename}(\emph{ident1},\emph{ident2}) : \textsf{void}\\
\end{center}
Parameters: 
\begin{itemize}
\item \emph{ident1} is the current name of the free variable.
\item \emph{ident2} is a fresh name.
\end{itemize}
\noindent Description: \begin{itemize}

\item \textbf{rename} permits a change of the name of the free variable. \sollya can handle
   only one free variable at a time. The first time in a session that an
   unbound name is used in a context where it can be interpreted as a free
   variable, the name is used to represent the free variable of \sollya. In the
   following, this name can be changed using \textbf{rename}.

\item Be careful: if \emph{ident2} has been set before, its value will be lost. Use
   the command \textbf{isbound} to know if \emph{ident2} is already used or not.

\item If \emph{ident1} is not the current name of the free variable, an error occurs.

\item If \textbf{rename} is used at a time when the name of the free variable has not been
   defined, \emph{ident1} is just ignored and the name of the free variable is set
   to \emph{ident2}.

\item It is always possible to use the special keyword \verb|_x_| to denote the free
   variable. Hence \emph{ident1} can be \verb|_x_|.

\item Notice that \emph{ident2} is an identifier, not a string. In particular, it is
   not directly possible to use the content of a variable to provide the new
   name of the free variable. This can however be obtained using a level of
   indirection: first create a string that contains the call to \textbf{rename}, and
   then evaluate it thanks to \textbf{parse} (see example below).
\end{itemize}
\noindent Example 1: 
\begin{center}\begin{minipage}{15cm}\begin{Verbatim}[frame=single]
> f=sin(x);
> f;
sin(x)
> rename(x,y);
> f;
sin(y)
\end{Verbatim}
\end{minipage}\end{center}
\noindent Example 2: 
\begin{center}\begin{minipage}{15cm}\begin{Verbatim}[frame=single]
> a=1;
> f=sin(x);
> rename(x,a);
> a;
a
> f;
sin(a)
\end{Verbatim}
\end{minipage}\end{center}
\noindent Example 3: 
\begin{center}\begin{minipage}{15cm}\begin{Verbatim}[frame=single]
> verbosity=1!;
> f=sin(x);
> rename(y, z);
Warning: the current free variable is named "x" and not "y". Can only rename the
 free variable.
The last command will have no effect.
> rename(_x_, z);
Information: the free variable has been renamed from "x" to "z".
\end{Verbatim}
\end{minipage}\end{center}
\noindent Example 4: 
\begin{center}\begin{minipage}{15cm}\begin{Verbatim}[frame=single]
> verbosity=1!;
> rename(x,y);
Information: the free variable has been named "y".
> isbound(x);
false
> isbound(y);
true
\end{Verbatim}
\end{minipage}\end{center}
\noindent Example 5: 
\begin{center}\begin{minipage}{15cm}\begin{Verbatim}[frame=single]
> verbosity=1!;
> f = sin(x);
> new_varname = "foo";
> rename(_x_, new_varname);
Information: the free variable has been renamed from "x" to "new_varname".
> f;
sin(new_varname)
> rename(_x_, x);
Information: the free variable has been renamed from "new_varname" to "x".
> new_varname = "foo";
> str = "rename(_x_, " @ new_varname @ ");";
> (parse("proc () { " @ str @ "}"))();
Information: the free variable has been renamed from "x" to "foo".
> f;
sin(foo)
\end{Verbatim}
\end{minipage}\end{center}
\noindent Example 6: 
\begin{center}\begin{minipage}{15cm}\begin{Verbatim}[frame=single]
> verbosity=1!;
> f = sin(x);
> f;
sin(x)
> rename(x, _x_);
Information: the free variable has been renamed from "x" to "_x_".
> f;
sin(_x_)
> g = cos(y);
> f;
sin(y)
> g;
cos(y)
> rename(_x_, _x_);
Information: the free variable has been renamed from "y" to "_x_".
> f;
sin(_x_)
> g;
cos(_x_)
\end{Verbatim}
\end{minipage}\end{center}
See also: \textbf{isbound} (\ref{labisbound}), \textbf{parse} (\ref{labparse})
